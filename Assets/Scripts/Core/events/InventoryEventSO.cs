﻿using System;
using Core.inventory;
using Core.inventory.InventoryView;
using UnityEngine;

namespace Core.events
{
    [CreateAssetMenu(menuName = "Event/Inventory Event Channel")]
    public class InventoryEventSO : ScriptableObject
    {
        public event Action<ItemStack> InventoryItemSlotEnterEvent;
        public event Action InventoryItemSlotLeaveEvent;
        public event Action<IInventoryView> InventoryClickEvent;
        public event Action<IInventoryView> InventoryOpenEvent;
        public event Action InventoryCloseEvent;
        public event Action<IPlayerInventory> PlayerInventoryUpdateEvent;

        public event Action<entity.Player, int, int> PlayerItemHeldEvent;

        public void OnInventoryClickEvent(IInventoryView inventoryView)
        {
            InventoryClickEvent?.Invoke(inventoryView);
        }

        public void OnInventoryOpenEvent(IInventoryView inventoryView)
        {
            InventoryOpenEvent?.Invoke(inventoryView);
        }

        public void OnInventoryCloseEvent()
        {
            InventoryCloseEvent?.Invoke();
        }

        public void OnInventoryItemSlotEnterEvent(ItemStack itemStack)
        {
            InventoryItemSlotEnterEvent?.Invoke(itemStack);
        }

        public void OnInventoryItemSlotLeaveEvent()
        {
            InventoryItemSlotLeaveEvent?.Invoke();
        }

        public void OnPlayerItemHeldEvent(entity.Player player, int previous, int current)
        {
            PlayerItemHeldEvent?.Invoke(player, previous, current);
        }

        public void OnInventoryUpdateEvent(IPlayerInventory playerInventory)
        {
            PlayerInventoryUpdateEvent?.Invoke(playerInventory);
        }
    }
}
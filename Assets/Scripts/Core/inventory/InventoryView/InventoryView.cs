﻿using Core.entity;
using UnityEngine;

namespace Core.inventory.InventoryView
{
    public sealed class InventoryView : MonoBehaviour, IInventoryView
    {
        [SerializeField] private InventoryViewProvider.InventoryViewProvider inventoryViewProvider;
        private ClickType _clickType;
        private ItemStack _cursorItemStack;
        private int _clickSlot;

        private IInventory _inventory;
        private InventoryAction _inventoryAction;
        private HumanEntity _viewer;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public InventoryAction GetAction()
        {
            return _inventoryAction;
        }

        public ClickType GetClick()
        {
            return _clickType;
        }

        public void SetCursor(ItemStack itemStack)
        {
            _cursorItemStack = itemStack;
        }

        public ItemStack GetCursor()
        {
            return _cursorItemStack;
        }

        public int GetSlot()
        {
            return _clickSlot;
        }

        public void SetSlot(int slot)
        {
            _clickSlot = slot;
        }

        public IInventory GetInventory()
        {
            return _inventory;
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }

        public HumanEntity GetPlayer()
        {
            return _viewer;
        }

        public void Init(IInventory inventory, HumanEntity viewer)
        {
            _inventory = inventory;
            _viewer = viewer;
            inventoryViewProvider.Init(this);
            gameObject.SetActive(true);
            _inventoryAction = InventoryAction.NOTHING;
            _clickType = ClickType.NONE;
        }

        public void SetAction(InventoryAction inventoryAction)
        {
            _inventoryAction = inventoryAction;
        }

        public void SetClickType(ClickType clickType)
        {
            _clickType = clickType;
        }
    }
}
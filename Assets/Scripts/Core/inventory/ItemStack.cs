using System;
using Core.inventory.meta;

namespace Core.inventory
{
    public class ItemStack : IComparable<ItemStack>
    {
        private readonly ItemMeta _itemMeta = new ItemMeta();
        private readonly EnumValue _enumValue;

        public ItemStack(EnumValue enumValue)
        {
            _enumValue = enumValue;
        }

        public int CompareTo(ItemStack other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var materialComparison = _enumValue.CompareTo(other._enumValue);
            if (materialComparison != 0) return materialComparison;
            var displayNameComparison = string.Compare(_itemMeta.GetDisplayName(), other._itemMeta.GetDisplayName(),
                StringComparison.Ordinal);
            if (displayNameComparison != 0) return displayNameComparison;
            return _itemMeta.GetAmount().CompareTo(other.GetItemMeta().GetAmount());
        }

        public new EnumValue GetType()
        {
            return _enumValue;
        }

        public ItemMeta GetItemMeta()
        {
            return _itemMeta;
        }
    }
}
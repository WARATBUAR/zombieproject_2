﻿namespace Core.inventory
{
    public interface IInventoryHolder<T> where T : IInventory
    {
        T GetInventory();
    }
}
﻿namespace Core.inventory
{
    public interface IPlayerInventory : IInventory
    {
        ItemStack GetItemInHand();
        void SetItemInHand(ItemStack itemStack);
        void SetHeldItemSlot(int slot);
        int GetHeldItemSlot();
    }
}
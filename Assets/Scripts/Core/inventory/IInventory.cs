﻿namespace Core.inventory
{
    public interface IInventory
    {
        ItemStack GetItem(int index);
        void SetItem(int index, ItemStack itemStack);
        void AddItem(ItemStack itemStack);
        void Remove(ItemStack itemStack);
        bool HasItem(ItemStack itemStack);
        int FirstEmpty();
        ItemStack[] GetContents();
        void SetStorageContents(ItemStack[] items);
        int GetSize();
        InventoryType GetType();
        bool IsEmpty();
    }
}
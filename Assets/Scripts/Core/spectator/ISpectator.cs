﻿using Cinemachine;

namespace Core.spectator
{
    public interface ISpectator
    {
        CinemachineVirtualCamera GetCamera();
    }
}
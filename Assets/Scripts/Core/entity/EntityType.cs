﻿namespace Core.entity
{
    public enum EntityType
    {
        ZOMBIE,
        DUMMY,
        TOWER,
        ZOMBIE_DOG,
        ZOMBIE_ROCK,
        ZOMBIE_GIANT,
        SOLDIER,
    }
}
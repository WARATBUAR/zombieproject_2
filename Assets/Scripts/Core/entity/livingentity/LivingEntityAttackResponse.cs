﻿using UnityEngine;

namespace Core.entity.livingentity
{
    public class LivingEntityAttackResponse : MonoBehaviour, ILivingEntityAttackResponse
    {
        [SerializeField] private float attackDelay;

        private float _timer;

        public void Attack(LivingEntity attacker, Entity target, float damage)
        {
            if (!(target is LivingEntity livingEntity)) return;
            //Look At Target
            var dir = target.GetLocation() - attacker.GetLocation();
            dir.y = 0f;
            dir.Normalize();
            var rotation = Quaternion.LookRotation(dir);
            attacker.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);

            _timer -= Time.deltaTime;
            if (_timer > 0) return;
            _timer = attackDelay;
            
            attacker.SwingHands();
            livingEntity.Damage(damage, attacker, false);
        }
    }
}
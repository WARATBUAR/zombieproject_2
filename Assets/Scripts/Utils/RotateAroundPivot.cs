﻿using UnityEngine;

namespace Utils
{
    public class RotateAroundPivot : MonoBehaviour
    {
        [SerializeField] private float rotateSpeed;
        [SerializeField] private Transform targetTransform;

        private void FixedUpdate()
        {
            transform.RotateAround(targetTransform.position, Vector3.up, rotateSpeed * Time.deltaTime);
        }
    }
}
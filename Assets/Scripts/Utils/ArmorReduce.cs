﻿using Core.armor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utils
{
    //NONE: No reduced damage
    //LOW: Reduced 1-30% damage
    //MEDIUM: Reduced 31-60% damage
    //HIGH: Reduced 61-90% damage
    //GREAT: Reduced 91+% damage
    //IMMUNE: All damage reduced

    public static class ArmorReduce
    {
        public static float GetReducedDamage(float damage, ArmorType armorType)
        {
            var reducedPercent = armorType switch
            {
                ArmorType.LOW => Random.Range(0.1f, 0.3f),
                ArmorType.MEDIUM => Random.Range(0.31f, 0.6f),
                ArmorType.HIGH => Random.Range(0.61f, 0.9f),
                ArmorType.GREATE => Random.Range(0.91f, 1f),
                ArmorType.IMMUNE => 1f,
                _ => 0
            };

            return Mathf.Clamp(damage - (damage * reducedPercent), 0f, damage);
        }
    }
}
﻿using System;
using UnityEngine;

namespace ScriptableObjects.Inventory
{
    [CreateAssetMenu(menuName = "Item/Item Icon Pack")]
    public class ItemIconPack : ScriptableObject
    {
        [SerializeField] private Sprite defaultIcon;
        [SerializeField] private EnumValue defaultMaterial;
        [SerializeField] private ItemIconData[] itemIconData;

        public Sprite GetItemIcon(EnumValue enumValue)
        {
            foreach (var data in itemIconData)
                if (data.enumValue.Equals(enumValue))
                    return data.icon;

            return defaultIcon;
        }

        public EnumValue GetDefault()
        {
            return defaultMaterial;
        }

        [Serializable]
        public struct ItemIconData
        {
            public EnumValue enumValue;
            public Sprite icon;
        }
    }
}
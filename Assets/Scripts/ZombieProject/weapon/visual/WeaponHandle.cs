﻿using Player.visual;
using UnityEngine;
using ZombieProject.events;

namespace ZombieProject.weapon.visual
{
    [RequireComponent(typeof(PlayerHand))]
    public class WeaponHandle : MonoBehaviour
    {
        [SerializeField] private float power;
        [SerializeField] private float startLength;
        [SerializeField] private float endLenght;

        private PlayerHand _playerHand;

        private void Awake()
        {
            _playerHand = GetComponent<PlayerHand>();
        }

        private void OnEnable()
        {
            GameEventInstance.Instance.WeaponEvent.ShootEvent += OnShootEvent;
        }

        private void OnDisable()
        {
            GameEventInstance.Instance.WeaponEvent.ShootEvent -= OnShootEvent;
        }

        private void OnShootEvent(IWeapon weapon)
        {
            if (!(weapon.GetShooter() is Core.entity.Player)) return;
            _playerHand.SwingHand(power, startLength, endLenght);
        }
    }
}
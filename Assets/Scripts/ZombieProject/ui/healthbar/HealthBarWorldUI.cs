﻿using Core.entity;
using Core.events;
using Core.ui;
using UnityEngine;
using ZombieProject.ui.progressbar;

namespace ZombieProject.ui.healthbar
{
    public class HealthBarWorldUI : DynamicBaseUI
    {
        [SerializeField] private EntityEventSO entityEventSo;
        private IProgressBar _progressBar;

        private LivingEntity _livingEntity;

        public override void Awake()
        {
            base.Awake();
            _progressBar = GetComponent<IProgressBar>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            entityEventSo.EntityDamageEvent += OnEntityDamageEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            entityEventSo.EntityDamageEvent -= OnEntityDamageEvent;
        }

        public void Init(LivingEntity livingEntity)
        {
            _livingEntity = livingEntity;
            _progressBar.SetProgress(livingEntity.GetHealth(), livingEntity.GetMaxHealth());
        }

        private void OnEntityDamageEvent(LivingEntity livingEntity, float damage)
        {
            if (livingEntity != _livingEntity) return;
            _progressBar.SetProgress(livingEntity.GetHealth(), livingEntity.GetMaxHealth());
        }
    }
}
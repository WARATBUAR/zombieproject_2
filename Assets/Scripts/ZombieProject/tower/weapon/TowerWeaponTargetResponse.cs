﻿using Core.entity;
using UnityEngine;
using ZombieProject.game.wave;

namespace ZombieProject.tower.weapon
{
    public class TowerWeaponTargetResponse : MonoBehaviour, ITowerWeaponTargetResponse
    {
        private Tower _tower;
        private ITowerWeapon _towerWeapon;
        private ITowerWeaponTargetProvider _towerWeaponTargetProvider;
        private IWaveSpawner _waveSpawner;

        private void Awake()
        {
            _tower = GetComponent<Tower>();
            _towerWeapon = GetComponent<ITowerWeapon>();
            _towerWeaponTargetProvider = GetComponent<ITowerWeaponTargetProvider>();
            _waveSpawner = FindObjectOfType<WaveSpawner>();
        }

        private void Start()
        {
            _towerWeapon.Init(_tower);
        }

        private void FixedUpdate()
        {
            _towerWeaponTargetProvider.FindTarget(_waveSpawner.GetEnemies().ToArray(),
                _towerWeapon.GetWeapon().GetWeaponSo().range, this);
        }

        public void OnTarget(LivingEntity target)
        {
            _towerWeapon.Attack(target);
        }
    }
}
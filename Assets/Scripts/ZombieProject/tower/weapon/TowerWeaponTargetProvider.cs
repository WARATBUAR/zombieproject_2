﻿using System.Collections.Generic;
using Core.entity;
using UnityEngine;

namespace ZombieProject.tower.weapon
{
    public class TowerWeaponTargetProvider : MonoBehaviour, ITowerWeaponTargetProvider
    {
        private LivingEntity _currentTarget;

        private void OnDisable()
        {
            _currentTarget = null;
        }

        public void FindTarget(IEnumerable<Mob> targets, float range, ITowerWeaponTargetResponse weaponTargetResponse)
        {
            if (HasTarget())
                if (!IsTargetDead(_currentTarget) && IsTargetInRange(_currentTarget, range))
                {
                    weaponTargetResponse.OnTarget(_currentTarget);
                    return;
                }

            //TARGET NOT IN RANGE OR DEAD
            _currentTarget = null;

            //FIND NEW TARGET
            foreach (var target in targets)
            {
                if (IsTargetDead(target)) continue;
                if (!IsTargetInRange(target, range)) continue;

                _currentTarget = target;
                weaponTargetResponse.OnTarget(target);
                break;
            }
        }

        private bool HasTarget()
        {
            return _currentTarget;
        }

        private bool IsTargetInRange(Entity target, float range)
        {
            return Vector3.Distance(target.GetLocation(), transform.position) < range;
        }

        private bool IsTargetDead(Entity target)
        {
            return target.IsDead() || !target.gameObject.activeSelf;
        }
    }
}
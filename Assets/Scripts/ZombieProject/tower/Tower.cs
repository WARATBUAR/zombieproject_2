﻿using Core.entity;
using Core.inventory;

namespace ZombieProject.tower
{
    public class Tower : Entity, IInventoryHolder<IInventory>
    {
        private IInventory _inventory;

        protected override void Awake()
        {
            base.Awake();
            _inventory = new Inventory(0, InventoryType.DEFAULT);
        }

        public IInventory GetInventory()
        {
            return _inventory;
        }
    }
}
﻿using System;
using UnityEngine;

namespace ZombieProject.game.specialitem.drone.events
{
    [CreateAssetMenu(menuName = "TD/Special Item Event", order = 0)]
    public class SpecialItemEventSo : ScriptableObject
    {
        public event Action<Drone> DroneActiveEvent;
        public event Action<Drone> DroneDeActiveEvent;
        public event Action<float, float> DroneTimerEvent;

        public void OnDroneActiveEvent(Drone drone)
        {
            DroneActiveEvent?.Invoke(drone);
        }

        public void OnDroneDeActiveEvent(Drone drone)
        {
            DroneDeActiveEvent?.Invoke(drone);
        }

        public void OnDroneTimerEvent(float timer, float delay)
        {
            DroneTimerEvent?.Invoke(timer, delay);
        }
    }
}
﻿using Core.entity;
using Core.entity.mob;
using Core.events;
using UnityEngine;
using ZombieProject.events;

namespace ZombieProject.game.wave.entity
{
    public class WaveDummy : MonoBehaviour
    {
        [SerializeField] private Transform dummySpawnPoint;
        private Dummy _dummy;
        private IWaveManager _waveManager;

        private void Awake()
        {
            _waveManager = FindObjectOfType<WaveManager>();
        }

        private void OnEnable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageEvent += OnEntityDamageEvent;
            GameEventInstance.Instance.GameEvent.WaveStartEvent += OnWaveStartEvent;
            GameEventInstance.Instance.GameEvent.WaveClearedEvent += OnWaveClearedEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageEvent -= OnEntityDamageEvent;
            GameEventInstance.Instance.GameEvent.WaveStartEvent -= OnWaveStartEvent;
            GameEventInstance.Instance.GameEvent.WaveClearedEvent -= OnWaveClearedEvent;
        }

        private void Start()
        {
            ShowDummy();
        }

        private void OnWaveClearedEvent(IWaveManager obj)
        {
            ShowDummy();
        }


        private void OnEntityDamageEvent(LivingEntity livingEntity, float damage)
        {
            if (!(livingEntity is Dummy dummy)) return;
            if (dummy != _dummy) return;
            _waveManager.NextWave();
            HideDummy();
        }

        private void OnWaveStartEvent(IWaveManager waveManager)
        {
            if (!IsDummyActive) return;
            HideDummy();
        }

        private bool IsDummyActive => _dummy;

        private void ShowDummy()
        {
            _dummy = (Dummy) EntityInstance.Instance.SpawnEntity(EntityType.DUMMY, dummySpawnPoint.position);
        }

        private void HideDummy()
        {
            if (!_dummy || _dummy.IsDead()) return;
            Destroy(_dummy.gameObject);
            _dummy = null;
        }
    }
}
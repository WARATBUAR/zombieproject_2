﻿using UnityEngine;
using ZombieProject.game.wave;

namespace ZombieProject.game.checkpoint
{
    [ExecuteInEditMode]
    public class MobCheckPoint : MonoBehaviour
    {
        public float mobPadding = 3f;
        public EntranceType entranceType;

        private void OnDrawGizmos()
        {
            switch (entranceType)
            {
                case EntranceType.A:
                    Gizmos.color = Color.green;
                    break;
                case EntranceType.B:
                    Gizmos.color = Color.cyan;
                    break;
                case EntranceType.C:
                    Gizmos.color = Color.red;
                    break;
                case EntranceType.D:
                    Gizmos.color = Color.magenta;
                    break;
            }

            var pos = transform.position;
            var padding = transform.right * mobPadding;
            Gizmos.DrawLine(pos, pos - padding);
            Gizmos.DrawLine(pos, pos + padding);
            Gizmos.DrawLine(pos, pos + transform.forward * mobPadding);
            Gizmos.DrawSphere(pos - padding, 0.5f);
            Gizmos.DrawSphere(pos + padding, 0.5f);
            Gizmos.DrawSphere(pos, 0.5f);
        }
    }
}